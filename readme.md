# BRUTE FORCE SKYWALKER

## Launch the program

To launch the program you need to be in the folder of the project and launch the command in terminal : `node index.js` then you will have an interactive prompt with followings questions : 

* The minimum length of the password
* The maximum length of the password
* The type of hack you want to use (Brute force/Dictionnary)

After you answer all the questions, the program is officially launch and will try to connect to the link that you specified in the code **(by default my wordpress web site)**.

## Brute force code
The brute force code is in **brute_force.js** file.

If you want to hack an account of another web site, you could change the url at the line 62 :     `await page.goto('http://13.36.251.95/wp-login.php',{waitUntil: 'networkidle0',});` **but you have to replace only "http://13.36.251.95/wp-login.php" by your own link.**

Be careful if you do this, you need to change the id of the username field at line 66 :  `await page.type('#user_login','nassimgc');` **because it's different for each web site.**

The same as well for the id of the passowrd field at line 28 : `await page.type('#user_pass',prefix);`

And finally, you need to change also the id of the submit button at line 30 : `await page.click('#wp-submit');`

## Dictionnary code
The dictionnary code is in **dictionnary.js** file.

If you want to use an other dictionnary file, you can add it to the project and change only the file name you want to use at line 11 : `let data = fs.readFileSync('dictionnary_folder/Passwords.dic','utf8');`. You need here to change "French.dic" by your own file name.

If you want to hack an account of another web site, you could change the url at the line 24 :     `await page.goto('http://13.36.251.95/wp-login.php',{waitUntil: 'networkidle0',});` **but you have to replace only "http://13.36.251.95/wp-login.php" by your own link.**

Be careful if you do this, you need to change the id of the username field at line 28 :  `await page.type('#user_login','nassimgc');` **because it's different for each web site.**

The same as well for the id of the passowrd field at line 48 : `await page.type('#user_pass',password[i]);`

And finally, you need to change also the id of the submit button at line 53 : `await page.click('#wp-submit');`.

## What library did I use to make this project ?

I used at the line 43 in the dictionnary code and at line 23 in the brute force code the function `waitForTimeOut`, because if the password was wrong the page was reloaded and it takes time to recharge everything properly and if i don't use this function, the program will submit the login form without type in the password field.

You can change the parameter value, in my project I put 1000ms by default.
## What library did I use to make this project ?
In this project, I use the library `readline-sync` for the interactive prompt and I also used `puppeteer` and `fs` for the dictionnary and brute_force.