const readlineSync = require('readline-sync');
//Import the file show for the Dictionnary attack function
const dictionnaryFunction = require("./dictionnary.js");
//Import the file show for the Brute force function
const bruteForceFunction = require("./brute_force.js");

async function main(){
    let i = 0;
    let minLength="";
    let maxLength="";

    //1st loop for the question about the length of the password
    while (i == 0){
        // Ask to the user the min length of the password
        minLength = readlineSync.question("What is the min length of the password ? ");
        // Ask to the user the max length of the password
        maxLength = readlineSync.question("What is the max length of the password ? ");

        // We check if the answer of the user are really number
        if(!isNaN(minLength) && !isNaN(maxLength)){
            // We convert to int the answer because readfileSync get the answer as string and not as number
            minLength = parseInt(minLength);
            maxLength = parseInt(maxLength);
            // We check if the maxLength is higher than minLength
            if(maxLength > minLength ){
                // We increment i to exit the loop for the question about the length of the password
                i++;
            }
        }else{
            // if answers are not number or maxLength is not higher than minLength we display an error message
            // And the user have to put good values
            console.log("Please select a valuable min and max length");
        }
    }
    // 2nd loop for the question about the type of hack the user want to use
    while(i != 2){
        let option = readlineSync.question("Choose an option of hack : \n"+
        "1 : Dictionnary attack \n"+" 2 : Brute force attack \n");

        // We check again if the answer is really a number
        if(!isNaN(option)){
            /*Then we use a switch case : 
            1 : is for the dictionnary
            2 : is for the brute force */
            switch (option) {
                case "1":
                  console.log("You choose the dictionnary attack");
                  // We call the function in dictionnary.js
                  await dictionnaryFunction.dictionnary(minLength,maxLength);
                  break;
                case "2":
                  console.log("You choose the brute force attack");
                  let str = "";
                  // We recover all UTF8 characters (maj, min, numbers, special characters) 
                  for(let i=33; i <= 126; i++){
                    str += String.fromCharCode(i);
                  }
                  // We call the function in brute_force.js
                  await bruteForceFunction.bruteForce(str,minLength,maxLength);
                  break;
                default:
                    // If the answer is not in switch case then display error
                  console.log("Fatal Error");
              }

            // We increment to exit the loop
            i++;
        }else{
            // We display an error message if the answer is not a number
            console.log("Please select a valuable option");
        }
        
    }
}

main();